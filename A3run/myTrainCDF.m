function [HMM] = myTrainCDF(M, Q, amount, dim)

addpath(genpath('/u/cs401/A3_ASR/code/FullBNT-1.0.7'));
train_dir = '/u/cs401/speechdata/Training';
speaker = dir(train_dir);
% remove . and .. directory
speaker = speaker(arrayfun(@(x) ~strcmp(x.name(1),'.'),speaker));
speaker_name = {speaker.name};

data = struct();
for i = 1:length(speaker_name) 
    fn_mfcc = dir([train_dir, filesep, speaker_name{i}, filesep, '*', 'mfcc']);
    fn_mfcc = {fn_mfcc.name};
    fn_phn = dir([train_dir, filesep, speaker_name{i}, filesep, '*', 'phn']);
    fn_phn = {fn_phn.name};
    for j = 1:length(fn_mfcc)
        mfccData = load([train_dir, filesep, speaker_name{i}, filesep, fn_mfcc{j}], 'mfcc');
        mfccData = mfccData(:, 1:dim);
        [Begins, Ends, Phones] = textread([train_dir, filesep, speaker_name{i}, filesep, fn_phn{j}], '%d %d %s', 'delimiter', '\n');
        for index = 1:length(Phones)
            % adjust 0-indexed -> 1-indexed
            myBegin = Begins(index)/128 + 1;
            myEnd = min(Ends(index)/128 + 1, length(mfccData));
            myPhone = char(Phones(index));
            if strcmp(myPhone, 'h#') == 1
                myPhone = 'sil';
            elseif strcmp(myPhone, 'ax-h') == 1
                myPhone = 'axDASH_h';
            elseif strcmp(myPhone, '1') == 1
                myPhone = 'ONE';
            elseif strcmp(myPhone, '2') == 1
                myPhone = 'TWO'; 
            end
                
            if ~isfield(data, myPhone)
                data.(myPhone){1} = mfccData(myBegin:myEnd,:)';
            else
                data.(myPhone){length(data.(myPhone)) + 1} = mfccData(myBegin:myEnd,:)';
            end
        end
    end                                               
end

initType = 'kmeans';
max_iter = 20;
allphns = fieldnames(data);
HMM = struct();
for i = 1:length(allphns)
    myPhone = allphns{i};
    % data.phone shuffle
    newlen = round(length(data.(myPhone)) * amount);
    rand_id = randperm(length(data.(myPhone)));
    data.(myPhone) = data.(myPhone)(rand_id);
    data.(myPhone) = data.(myPhone)(:,1:newlen);
    HMM.(myPhone) = initHMM(data.(myPhone), M, Q, initType);
    [HMM.(myPhone), LL] = trainHMM( HMM.(myPhone), data.(myPhone), max_iter);
end

return
