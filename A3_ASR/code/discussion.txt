A	Discussion for Q2
==========================
For 2.1, 2.2, and 2.3, run 'run_Q2.m' to get the results.

We have experimented with different settings of M ,ε, and S. The number of maximum iterations is set to be 50. The results are included in 2.3experiment.txt and 2.3experiment_S.txt. It should be noted that only the IDs of 15 test utterances are provided whereas those of the other 15 utterances are missing. Therefore only the 15 utterances with IDs given are taken into account for the calculation of accuracy.

1 We have experimented with the settings of M = [2, 4, 8, 12] and epsilon = [0.1, 0.001, 0.00001]. The results are included in 2.3experiment.txt. 

For example, when epsilon is fixed as 0.001, the classification accuracy vs M is obtained as follows:
epsilon		M		accuracy
=====================================================
0.001000	2		0.933
0.001000	4		0.867
0.001000	8		1.000
0.001000	12		1.000

It is found that the classification accuracy increases with the increase of M. This phenomenon is reasonable because a smaller number of gaussians may result in underfitting, hence reducing the accuracy. A larger M may make the model more accurate. However, the classification accuracy may drop due to overfitting when M is too large.

When M is fixed as 4, the classification vs accuracy is obtained as follows:
epsilon		M		accuracy
=====================================================
0.100000	4		1.000
0.001000	4		0.867
0.000010	4		0.933

Obviously, the classification accuracy decreases as the increase of ε.This is because when epsilon is larger, it becomes more difficult to reach the global minima.The training may stop before getting converged and result in underfittig as well. However, because only half of the test utterances are taken into account for accuracy calculation, there may be difference between the results and the overall accuracy, although the trend of accuracy is worth studying.

2 We have also experimented with deltaS = [4, 8, 16, 20]. deltaS represents the decrease of number of speakers. For each case, a total number of deltaS speakers with random indices are excluded from the training set. M and epsilon are fixed as 8 and 0.00001, respectively. The results are included in 2.3experiment_S.txt as follows:

deltaS	epsilon		M		accuracy
=====================================================
4	0.000010	8		0.933
8	0.000010	8		0.800
16	0.000010	8		0.600
20	0.000010	8		0.600

It is obvious that the accuracy decreases as the increase of deltaS. This is because when there are fewer speakers in the training set, it is more possible that the speakers in the test set are not included in the training set, which results in low accuracy.

3 To improve the classification accuracy of the Gaussian mixtures, one may choose appropriate M, which should be large enough to fit the model whereas not too large to prevent overfitting. This can be facilitated by doing cross-validation. Meanwhile, epsilon should be small enough to reach a minima. Alternatively, start by k-means to speed up convergence and improve accuracy.

4 When the maximum log likelihood of a test utterance is very low, we may decide it comes from none of the speakers in the training set. This can be done by setting up a threshold, which may be observed by comparing the log likelihoods with and without certain speakers. When the log likelihood is below the threshold, we may conclude the utterance comes from none of the speakers in the training set. 

5 Some alternative methods for doing speaker identification are SVM, neural networks, hidden markov models, etc.  

————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


B	Discussion for Q3
=========================

**********
*Part 3.1*
**********

*To execute code quickly, you can run myRun.m with the HMM.mat provided (which is trained by myTrain.m)*

Parameters: 		M = 8, Q = 3, max iteration = 20, amount = all, dimension = 14.
Besides, the mfccdata of each phone are randomized after gathering all the examples, so it’s not one speaker after another, but still keeps the order of each specific phone.
Overall Accuracy:  	45.71% 

**********
*Part 3.2*
**********

a) First, we started with control variate method based on default settings for all parameters as follows:
M = 8 (number of mixtures per state)
Q = 3 (number of states per sequence)
amount = all (the amount of training data used — randomly chosen)
dimension = 14 (first d dimensions of the mdcc data)
We made change to one parameter each time with max iteration = 10 and 20, respectively.

For max iteration = 10:		For max iteration = 20:

Try different M:
M = 5: 46.62%				M = 5: 46.72% 
M = 4: 46.75%				M = 4: 47.53%
M = 3: 47.26%				M = 3: 46.62%
Comments: The accuracy increases as the number of mixtures per state decreases from 8 to the smaller numbers, reaching the maximum at M = 3(iter = 10) and M = 4(iter = 20). We can infer that when M = 8, our training model might be overfitting. But if we continue to decrease M, there might be an underfitting problem.

Try different Q:
Q = 2: 46.35%				Q = 2: 46.44%
Q = 1: 45.07%				Q = 1: 46.35%
Comments: From both sets of parameters, we can see that when Q = 2, the model has the highest accuracy on test data.

Try different Amount:
Amount = 2/3: 41.04%			Amount = 2/3: 41.26%
Amount = 1/2: 37.41%			Amount = 1/2: 37.41%
Comments: The accuracy decreases significantly when we reduced the amount of training data due to underfitting problem.

Try different DIM:
DIM = 10: 44.62%			DIM = 10: 43.98%
DIM = 8: 39.34%				DIM = 8: 39.57%
DIM = 5: 35.22%				DIM = 5: 35.77%
Comments: The accuracy decreases when we removed some features from the mfcc data (may have removed some important features which contained huge information.)

Overall comments: Compared with different iterations, 20 iterations seems to perform slightly better than 10. However, it still can reach local optima, which can be the possible explanation for the M experiments.

b) Experiment on other different scenarios   
Parameter set: M = [4,3] Q = [2,1] A = [1,2/3] D = [14,10]

M	Q	A	D	Accuracy
———————————————————————————————————
4	2	1	14	48.07%
4	2	1	10	46.53%
4	2	2/3	14	43.16%
4	2	2/3	10	44.46%
———————————————————————————————————
4	1	1	14	46.13%
4	1	1	10	42.24%
4	1	2/3	14	42.43%
4	1	2/3	10	40.05%
———————————————————————————————————
3	2	1	14	46.68%
3	2	1	10	44.63%
3	2	2/3	14	42.97%
3	2	2/3	10	39.12%	
———————————————————————————————————
3	1	1	14	45.93%
3	1	1	10	42.01%
3	1	2/3	14	41.87%
3	1	2/3	10	38.32%

We can clearly see from the table above that [M, Q, A, D] = [4, 2, 1, 14] gives the highest accuracy on the test set, which is 48.07%.
When M and Q keep unchanged, the amount of the training data seems to have more impact on the final result than the dimension. Accuracies of each group split by underscore are generally decreasing.

**********
*Part 3.3*
**********

Reference: Now here is truly a marvel. 
Hypothsis: Now here is truly hey marvel. 
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
=============================================
Reference: The cartoon features a muskrat and a tadpole. 
Hypothsis: Cat tune features a muskrat and a tadpole. 
SE: 0.250000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.250000 
=============================================
Reference: Just let me die in peace. 
Hypothsis: Just let me die in peace. 
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The sculptor looked at him, bugeyed and amazed, angry. 
Hypothsis: The sculptor looked at him, bug I'd and amazed, angry. 
SE: 0.111111  IE: 0.111111  DE: 0.000000 
LD for each sentence: 0.222222 
=============================================
Reference: A flash illumined the trees as a crooked bolt twigged in several directions. 
Hypothsis: A flash illuminated the trees as crook bolt tweaked several directions. 
SE: 0.230769  IE: 0.000000  DE: 0.153846 
LD for each sentence: 0.384615 
=============================================
Reference: This is particularly true in site selection. 
Hypothsis: This is particularly true sight selection. 
SE: 0.142857  IE: 0.000000  DE: 0.142857 
LD for each sentence: 0.285714 
=============================================
Reference: We would lose our export markets and deny ourselves the imports we need. 
Hypothsis: We would lose sour expert markets deny ourselves the imports we need. 
SE: 0.153846  IE: 0.000000  DE: 0.076923 
LD for each sentence: 0.230769 
=============================================
Reference: Count the number of teaspoons of soysauce that you add. 
Hypothsis: Compton number of teaspoons of so he sauce that you add. 
SE: 0.200000  IE: 0.200000  DE: 0.100000 
LD for each sentence: 0.500000 
=============================================
Reference: Finally he asked, do you object to petting? 
Hypothsis: Finally he asked, do you object to petting? 
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Draw every outer line first, then fill in the interior. 
Hypothsis: Draw every other line first, then fill into interior. 
SE: 0.200000  IE: 0.000000  DE: 0.100000 
LD for each sentence: 0.300000 
=============================================
Reference: Change involves the displacement of form. 
Hypothsis: Change involves the displacement of fawn. 
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
=============================================
Reference: To his puzzlement, there suddenly was no haze. 
Hypothsis: Two is puzzle mint, there suddenly was no haze. 
SE: 0.375000  IE: 0.125000  DE: 0.000000 
LD for each sentence: 0.500000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: Donna's me to carry oily rag like that. 
SE: 0.100000  IE: 0.000000  DE: 0.200000 
LD for each sentence: 0.300000 
=============================================
Reference: The full moon shone brightly that night. 
Hypothsis: The the full moon shone brightly that night. 
SE: 0.000000  IE: 0.142857  DE: 0.000000 
LD for each sentence: 0.142857 
=============================================
Reference: Tugboats are capable of hauling huge loads. 
Hypothsis: Tugboats are capable falling huge loads. 
SE: 0.142857  IE: 0.000000  DE: 0.142857 
LD for each sentence: 0.285714 
=============================================
Reference: Did dad do academic bidding? 
Hypothsis: Did tatoo academic bidding? 
SE: 0.200000  IE: 0.000000  DE: 0.200000 
LD for each sentence: 0.400000 
=============================================
Reference: She had your dark suit in greasy wash water all year. 
Hypothsis: See add your dark suit and greasy wash water all year. 
SE: 0.272727  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.272727 
=============================================
Reference: The thick elm forest was nearly overwhelmed by Dutch Elm Disease. 
Hypothsis: The thick forest was nearly over helmed by Dutch Elm Disease. 
SE: 0.090909  IE: 0.090909  DE: 0.090909 
LD for each sentence: 0.272727 
=============================================
Reference: Count the number of teaspoons of soysauce that you add. 
Hypothsis: Cow ten number of teaspoons of soysauce that you add. 
SE: 0.200000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: Norwegian sweaters are made of lamb's wool. 
Hypothsis: Norwegian sweaters are made of lamb's wool. 
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: We think differently. 
Hypothsis: We think differently. 
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: A toothpaste tube should be squeezed from the bottom. 
Hypothsis: A too pays too should be squeezed from the bottom. 
SE: 0.222222  IE: 0.111111  DE: 0.000000 
LD for each sentence: 0.333333 
=============================================
Reference: Ran away on a black night with a lawful wedded man. 
Hypothsis: Ran away on a black night with an awful wedded man. 
SE: 0.181818  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.181818 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: Down ask me to carry an oily rag like that. 
SE: 0.100000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.100000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: Don't ask me to carry an oily rag like that. 
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Index words and electronic switches may be reserved in the following ways. 
Hypothsis: Index words an electronic switches may be reserved in the following way. 
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
=============================================
Reference: The avalanche triggered a minor earthquake. 
Hypothsis: The avalanche triggered minor earth way. 
SE: 0.166667  IE: 0.166667  DE: 0.166667 
LD for each sentence: 0.500000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: Donna's me to carry an oily rag like that. 
SE: 0.100000  IE: 0.000000  DE: 0.100000 
LD for each sentence: 0.200000 
=============================================
Reference: The thick elm forest was nearly overwhelmed by Dutch Elm Disease. 
Hypothsis: The thick elm for his was nail he over well bye touch Elm Disease. 
SE: 0.454545  IE: 0.272727  DE: 0.000000 
LD for each sentence: 0.727273 
=============================================
Reference: When all else fails, use force. 
Hypothsis: When hall else fails, use forks. 
SE: 0.333333  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.333333 
=============================================
SE overall: 0.165385
IE overall: 0.042308
DE overall: 0.050000
WER overall: 0.257692


————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


C	Discussion for Q4
=========================

**********
*Part 4.1*
**********

Reference: Now here is truly a marvel. 
Hypothsis: now here is truly a Marvel  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The cartoon features a muskrat and a tadpole. 
Hypothsis: captain features a muskrat and a tadpole  
SE: 0.125000  IE: 0.000000  DE: 0.125000 
LD for each sentence: 0.250000 
=============================================
Reference: Just let me die in peace. 
Hypothsis: just let me die in peace  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The sculptor looked at him, bugeyed and amazed, angry. 
Hypothsis: the sculptor looked at him bug eyed and amazed angry  
SE: 0.111111  IE: 0.111111  DE: 0.000000 
LD for each sentence: 0.222222 
=============================================
Reference: A flash illumined the trees as a crooked bolt twigged in several directions. 
Hypothsis: flash live in the trees as a cricket ball twig in several directions  
SE: 0.461538  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.461538 
=============================================
Reference: This is particularly true in site selection. 
Hypothsis: this is particularly true in site selection  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: We would lose our export markets and deny ourselves the imports we need. 
Hypothsis: we would lose our export markets and deny ourselves imports we need  
SE: 0.000000  IE: 0.000000  DE: 0.076923 
LD for each sentence: 0.076923 
=============================================
Reference: Count the number of teaspoons of soysauce that you add. 
Hypothsis: continental have teaspoons of soy sauce that you add  
SE: 0.500000  IE: 0.000000  DE: 0.100000 
LD for each sentence: 0.600000 
=============================================
Reference: Finally he asked, do you object to petting? 
Hypothsis: finally he asked do you object to petting  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Draw every outer line first, then fill in the interior. 
Hypothsis: try every other line first then fill in the interior  
SE: 0.200000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: Change involves the displacement of form. 
Hypothsis: change involves the displacement of femme  
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
=============================================
Reference: To his puzzlement, there suddenly was no haze. 
Hypothsis: to his puzzlement there suddenly was no Hayes  
SE: 0.125000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.125000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: don't ask me to carry read like that  
SE: 0.100000  IE: 0.000000  DE: 0.200000 
LD for each sentence: 0.300000 
=============================================
Reference: The full moon shone brightly that night. 
Hypothsis: full moon shone brightly that night  
SE: 0.142857  IE: 0.000000  DE: 0.142857 
LD for each sentence: 0.285714 
=============================================
Reference: Tugboats are capable of hauling huge loads. 
Hypothsis: tug boats a capable of hauling huge loads  
SE: 0.285714  IE: 0.142857  DE: 0.000000 
LD for each sentence: 0.428571 
=============================================
Reference: Did dad do academic bidding? 
Hypothsis: did dad do academic betting  
SE: 0.200000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: She had your dark suit in greasy wash water all year. 
Hypothsis: she had your dark suit increase you wash water all year  
SE: 0.181818  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.181818 
=============================================
Reference: The thick elm forest was nearly overwhelmed by Dutch Elm Disease. 
Hypothsis: the thick el virus was nearly overwhelmed by Dutch elm disease  
SE: 0.181818  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.181818 
=============================================
Reference: Count the number of teaspoons of soysauce that you add. 
Hypothsis: cop number of teaspoons of soy sauce that you had  
SE: 0.300000  IE: 0.100000  DE: 0.100000 
LD for each sentence: 0.500000 
=============================================
Reference: Norwegian sweaters are made of lamb's wool. 
Hypothsis: waging sweaters are made of lambs will  
SE: 0.285714  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.285714 
=============================================
Reference: We think differently. 
Hypothsis: we think differently  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: A toothpaste tube should be squeezed from the bottom. 
Hypothsis: a toothpaste tube should be squeezed from the bottom  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Ran away on a black night with a lawful wedded man. 
Hypothsis: ran away a black knight with a lawful wedded man  
SE: 0.090909  IE: 0.000000  DE: 0.090909 
LD for each sentence: 0.181818 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: don't ask me to carry an oily rag like that  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: town asked me to carry an oily rag like that  
SE: 0.200000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: Index words and electronic switches may be reserved in the following ways. 
Hypothsis: index words and electronic switches may be reserved in the following ways  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The avalanche triggered a minor earthquake. 
Hypothsis: yeah avalanche triggered a minor earthquake  
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: don't ask me to carry an oily rag like that  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The thick elm forest was nearly overwhelmed by Dutch Elm Disease. 
Hypothsis: the thick and Forrest was nearly overwhelmed by Dutch elm disease  
SE: 0.181818  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.181818 
=============================================
Reference: When all else fails, use force. 
Hypothsis: when all else fails use force  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
SE overall: 0.142308
IE overall: 0.011538
DE overall: 0.030769
WER overall: 0.184615

**********
*Part 4.2*
**********

Reference: Now here is truly a marvel. 
Hypothsis: now here is truly of Marvel  
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
===========
Reference: The cartoon features a muskrat and a tadpole. 
Hypothsis: the cartoon features of muskrat and a tadpole  
SE: 0.125000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.125000 
=============================================
Reference: Just let me die in peace. 
Hypothsis: just let me die in peace  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The sculptor looked at him, bugeyed and amazed, angry. 
Hypothsis: the sculptor looked at him Eugene amazed angry  
SE: 0.111111  IE: 0.000000  DE: 0.111111 
LD for each sentence: 0.222222 
=============================================
Reference: A flash illumined the trees as a crooked bolt twigged in several directions. 
Hypothsis: a flash illumined the trees is a cricket ball twig in several directions  
SE: 0.307692  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.307692 
=============================================
Reference: This is particularly true in site selection. 
Hypothsis: this is particularly true in site selection  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: We would lose our export markets and deny ourselves the imports we need. 
Hypothsis: we would lose our export markets and deny ourselves the imports we need  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Count the number of teaspoons of soysauce that you add. 
Hypothsis: count the number of teaspoons of soy sauce that you add  
SE: 0.100000  IE: 0.100000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: Finally he asked, do you object to petting? 
Hypothsis: finally he asked do you object to petting  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Draw every outer line first, then fill in the interior. 
Hypothsis: draw every other line first then fill in the interior  
SE: 0.100000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.100000 
=============================================
Reference: Change involves the displacement of form. 
Hypothsis: change involves the displacement of forum  
SE: 0.166667  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.166667 
=============================================
Reference: To his puzzlement, there suddenly was no haze. 
Hypothsis: to his puzzlement there suddenly was no Hayes  
SE: 0.125000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.125000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: Dante asked me to carry an oily rag like that  
SE: 0.200000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: The full moon shone brightly that night. 
Hypothsis: the full moon shone brightly that night  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Tugboats are capable of hauling huge loads. 
Hypothsis: tug boats are capable of hauling huge loads  
SE: 0.142857  IE: 0.142857  DE: 0.000000 
LD for each sentence: 0.285714 
=============================================
Reference: Did dad do academic bidding? 
Hypothsis: did dad do academic bidding  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: She had your dark suit in greasy wash water all year. 
Hypothsis: she had your dark suit in greasy wash water all year  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The thick elm forest was nearly overwhelmed by Dutch Elm Disease. 
Hypothsis: the thick around forest was nearly overwhelmed by Dutch elm disease  
SE: 0.090909  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.090909 
=============================================
Reference: Count the number of teaspoons of soysauce that you add. 
Hypothsis: count the number of teaspoons of soy sauce that you add  
SE: 0.100000  IE: 0.100000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: Norwegian sweaters are made of lamb's wool. 
Hypothsis: Norwegian sweaters are made of lambs will  
SE: 0.142857  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.142857 
=============================================
Reference: We think differently. 
Hypothsis: we think differently  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: A toothpaste tube should be squeezed from the bottom. 
Hypothsis: a toothpaste tube should be squeezed from the bottom  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Ran away on a black night with a lawful wedded man. 
Hypothsis: ran away on a black knight with a lawful wedded man  
SE: 0.090909  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.090909 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: dont ask me to carry an oily rag like that  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: Dante asked me to carry an oily rag like that  
SE: 0.200000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.200000 
=============================================
Reference: Index words and electronic switches may be reserved in the following ways. 
Hypothsis: index words and electronic switches may be reserved in the following ways  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The avalanche triggered a minor earthquake. 
Hypothsis: the avalanche triggered a minor earthquake  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: Don't ask me to carry an oily rag like that. 
Hypothsis: dont ask me to carry an oily rag like that  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: The thick elm forest was nearly overwhelmed by Dutch Elm Disease. 
Hypothsis: the thick elm forest was nearly overwhelmed by Dutch elm disease  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
Reference: When all else fails, use force. 
Hypothsis: when all else fails use force  
SE: 0.000000  IE: 0.000000  DE: 0.000000 
LD for each sentence: 0.000000 
=============================================
SE overall: 0.076923
IE overall: 0.011538
DE overall: 0.003846
WER overall: 0.092308

Comparison of the results across both sub-experiments:
The WER for synthesized utterances is almost half of that for original utterances. The possible explanation maybe that the audio files in 4-2 were generated by IBM’s models, which might produce a better result for later speech-to-text due to consistency of models. Moreover, we also tried to use default voice (en-US_MichaelVoice) for all the utterances during the text-to-speech process, and it even has a lower overall WER (0.078341).

————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


D	Discussion for bonus
============================
1 Voice banking
username: wwli194, iris1024

2 PCA
We have applied PCA to Q2 speaker identification. Results can be obtained by running ‘run_bonus_PCA.m’.

We have explored pca_N = [12, 8, 6, 4], where pca_N is the reduced dimensionality. 

The classification accuracy for different M, epsilon, and pca_N is shown in 2.3experiment_pca.txt. The accuracy decreases as pca_N decreases, although this is only obvious when M is smaller than 8. When the dimensionality is reduced to 6, the accuracy drops significantly for all the combinations of M and epsilon. For each pca_N, the corresponding explained variance proportion, which is the ratio between the sums of eigenvalues retained in PCA and all the eigenvalues, is also shown in the results. When pca_N is larger than 8, the proportion is larger than 0.98, meaning that most of the variance in the original data is retained. Therefore, pca_N = 8 is chosen as an optimal value. 

We set M = 8, epsilon = 0.00001, pca_N = 8 and explored different deltaS, i. e. number of speakers in the training set. The classification accuracy for different deltaS is shown as follows (included in 2.3experiment_S_pca.txt):

deltaS	dimension	epsilon		M		accuracy	explained_ratio
======================================================================================
4	8		0.000010	8		1.000		0.980
8	8		0.000010	8		0.733		0.981
16	8		0.000010	8		0.533		0.980
20	8		0.000010	8		0.533		0.980


It can be observed that the accuracy drops significantly when the number of speakers is reduced. This is consistent with the result without PCA pre-processing. When 16 (over half) speakers are excluded, the accuracy even drops to 53.3 %. This is because most of the speakers in the test set are excluded from the training set, resulting in incorrect classifications.
