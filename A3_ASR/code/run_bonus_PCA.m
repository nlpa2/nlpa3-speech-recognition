% run the following code for the bonus part
dir_train = '/u/cs401/speechdata/Training';
dir_test = '/u/cs401/speechdata/Testing';
max_iter = 50; % max_iter is fixed

% PCA with different epsilon, M, and pca_N. Results are written into '2.3experiment_pca.txt'
epsilon = [0.1, 1e-3, 1e-5];
M = [2, 4, 8, 12];
pca_N = [12, 8, 6, 4];
gmms_pca = parametric_experiment_pca(dir_train, dir_test, max_iter, epsilon, M, pca_N);

% fix M, epsilon, and pca_N, and explore different deltaS.
% Results are written into '2.3experiment_S_pca.txt'
deltaS = [4, 8, 16, 20];
M = 8;
epsilon = 1e-5;
pca_N = 8;
accuracy = parametric_experiment_pca(dir_train, dir_test, max_iter, epsilon, M, pca_N, deltaS);