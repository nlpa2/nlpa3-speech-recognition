function [class_result, accuracy]= gmmClassify_pca(dir_test, M, gmms, PCvec)
% gmmClassify_pca
% this function applies PCA to the test utterance and does classification based on that
% input:    dir_test    :   directory of test utterances (string)
%           M           :   number of gaussian models (integer)
%           gmms        :   trained gaussian mixture models (cell array of struct)
%           PCvec       :   projection matrix of PCA (matrix)  with dimensionality of d'*T
%
% output:   class_result:   classification results, i. e. recoginzed speakers for all the test utterances
%                           (cell of strings)
%           accuracy    :   classificaiton accuracy 
    if nargin ~= 4
        disp('Please input 4 arguments dir_test, M, gmms, PCvec.');
        return;
    end
    test_label = generate_test_label(dir_test);
    DD_mfcc = dir([dir_test, filesep, '*.mfcc']);
    class_result = cell(length(DD_mfcc), 1);
    sum = 0;
    total_num = 0;
    for index=1:length(DD_mfcc)
        X_matrix = loadmfcc(DD_mfcc(index), dir_test);
        X_matrix_pca = X_matrix * PCvec; 
        P = cell([length(gmms), 2]);  % index and log likelihood of utterance uttered by each speaker
        for i=1:length(gmms)
            P{i, 1} = LogLike(X_matrix_pca, M, gmms{i});
            P{i, 2} = gmms{i}.name;
        end
        P = sortrows(P, -1);
        class_result{index} = P{1, 2};
        % only the IDs included in TestingID file is taken in to account for accuracy calculation
        if isfield(test_label, DD_mfcc(index).name(1:end-5))
            sum = sum + strcmp(class_result{index}, test_label.(DD_mfcc(index).name(1:end-5)));
            total_num = total_num + 1;
        end
    end
    accuracy = sum / total_num;
    
end
