addpath(genpath('/u/cs401/A3_ASR/code/FullBNT-1.0.7'));
test_dir = '/u/cs401/speechdata/Testing';
% load HMM models
fn_HMM = 'HMM.mat';
load(fn_HMM);
hmm_phns = fieldnames(HMM);
mfccFile = dir([test_dir, filesep, '*', 'mfcc']);
mfccFile = {mfccFile.name};
phnFile = dir([test_dir, filesep, '*', 'phn']);
phnFile = {phnFile.name};

dim = 14;
data = struct();
% correct is the number of correctly identified phonemes
correct= 0;
% all_phns is the total number of phonemes
all_phns = 0;
f = fopen('./3.1output.txt','w');
for i = 1:length(mfccFile)
    mfccData = load([test_dir, filesep, mfccFile{i}],'mfcc');
    % load mfccData regarding to the dimension we need
    mfccData = mfccData(:, 1:dim);
    % read data from phn file: [begin][end][phone]
    [Begins, Ends, Phones] = textread([test_dir, filesep, phnFile{i}], '%d %d %s', 'delimiter', '\n');
    % store the predicted phoneme and expected phoneme as a pair
    phn_compares = {};
    for index = 1:length(Phones)
        % adjust 0-indexed -> 1-indexed
        myBegin = Begins(index)/128 + 1;
        % the last end in phn file may exceed that one in mfcc file
        myEnd = min(Ends(index)/128 + 1, length(mfccData));
        % convert phoneme to a char array
        myPhone = char(Phones(index));
        % replace #_12(cannot be a valid struct field) with other symbols
        if strcmp(myPhone, 'h#') == 1
            myPhone = 'sil';
        elseif strcmp(myPhone, 'ax-h') == 1
            myPhone = 'axDASH_h';
        elseif strcmp(myPhone, '1') == 1
            myPhone = 'ONE';
        elseif strcmp(myPhone, '2') == 1
            myPhone = 'TWO'; 
        end
        
        % initialize the max likelihood with -Inf
        LL_MAX = log(0);
        % traverse all the phonemes to find the maximum LL and
        % corresponding phoneme
        for j = 1:length(hmm_phns)
            to_compute_phn = char(hmm_phns(j));
            to_compute_data = mfccData(myBegin:myEnd,:)';
            LL = loglikHMM(HMM.(to_compute_phn), to_compute_data);
            % keep track of the maximum value
            if LL > LL_MAX
                LL_MAX = LL;
                phn_compares{index} = {myPhone, to_compute_phn};
            end
        end
        disp(['Expected: ',phn_compares{index}{1},' Predicted: ',phn_compares{index}{2}]);
        fprintf(f,['Expected: ',phn_compares{index}{1},' Predicted: ',phn_compares{index}{2},'\n']);
    end
    % calculate accuracy for each utterance
    correct_per_file = sum(cellfun(@(s) strcmp(s{1},s{2}), phn_compares));
    accuracy_per_file = correct_per_file / length(Phones);
    % accumulate correctness and total numbers
    correct = correct + correct_per_file;
    all_phns = all_phns + length(Phones);
    disp(['File No: ', num2str(i), ' Accuracy: ', num2str(accuracy_per_file)]);
    disp('----------------------------------------------');
    fprintf(f,'Accuracy of file No.%d: %.2f%%', i, accuracy_per_file * 100);
    fprintf(f,'\n----------------------------------------------\n');
end

% compute total accuracy for all the test data
total_accuracy = correct / all_phns;
fprintf(f,'******************* Total Accuracy: %.2f%% *******************', total_accuracy * 100);
fclose(f);
                

