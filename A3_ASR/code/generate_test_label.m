function test_label = generate_test_label(dir_test)
% generate_test_label
% this function generation the a struct named test_label from the given TestingIDs1-15.txt file
% input: dir_test    : the directory of Testing set (string)
% output test_label  : a struct of speaker file name and test index: e. g. test_label.unkn_1 = MMRP0

     test_label = struct();
     path_l = [dir_test, filesep, 'TestingIDs1-15.txt'];
     data = importdata(path_l);
     for i=2:length(data)
         match = regexp(data{i}, '^(\d+)\s*:\s*(\w+\d*):', 'tokens');
          filename = strcat('unkn_', match{1}{1});
          test_label.(filename) = match{1}{2};
     end
end