addpath(genpath('/u/cs401/A3_ASR/code/FullBNT-1.0.7'));
train_dir = '/u/cs401/speechdata/Training';
speaker = dir(train_dir);
% remove . and .. directory
speaker = speaker(arrayfun(@(x) ~strcmp(x.name(1),'.'),speaker));
speaker_name = {speaker.name};

% default dim value is 14
dim = 14;
% store each phoneme in a structure
data = struct();

for i = 1:length(speaker_name)
    % get all mfcc file names
    fn_mfcc = dir([train_dir, filesep, speaker_name{i}, filesep, '*', 'mfcc']);
    fn_mfcc = {fn_mfcc.name};
    % get all phn file names
    fn_phn = dir([train_dir, filesep, speaker_name{i}, filesep, '*', 'phn']);
    fn_phn = {fn_phn.name};
    for j = 1:length(fn_mfcc)
        mfccData = load([train_dir, filesep, speaker_name{i}, filesep, fn_mfcc{j}], 'mfcc');
        % load mfccData regarding to the dimension we need
        mfccData = mfccData(:, 1:dim);
        % read data from phn file: [begin][end][phone]
        [Begins, Ends, Phones] = textread([train_dir, filesep, speaker_name{i}, filesep, fn_phn{j}], '%d %d %s', 'delimiter', '\n');
        for index = 1:length(Phones)
            % adjust 0-indexed -> 1-indexed
            myBegin = Begins(index)/128 + 1;
            % the last end in phn file may exceed that one in mfcc file
            myEnd = min(Ends(index)/128 + 1, length(mfccData));
            % convert phoneme to a char array
            myPhone = char(Phones(index));
            % replace #_12(cannot be a valid struct field) with other symbols
            if strcmp(myPhone, 'h#') == 1
                myPhone = 'sil';
            elseif strcmp(myPhone, 'ax-h') == 1
                myPhone = 'axDASH_h';
            elseif strcmp(myPhone, '1') == 1
                myPhone = 'ONE';
            elseif strcmp(myPhone, '2') == 1
                myPhone = 'TWO'; 
            end
            
            % store mfccData corresponding to each phoneme
            if ~isfield(data, myPhone)
                data.(myPhone){1} = mfccData(myBegin:myEnd,:)';
            else
                data.(myPhone){length(data.(myPhone)) + 1} = mfccData(myBegin:myEnd,:)';
            end
        end
    end                                               
end

% some default values
M = 8;
Q = 3;
initType = 'kmeans';
max_iter = 20;
amount = 1;
HMM = struct();
allphns = fieldnames(data);
for i = 1:length(allphns)
    myPhone = allphns{i};
    % data.phone shuffle, amount can change the training dataset size
    newlen = round(length(data.(myPhone)) * amount);
    rand_id = randperm(length(data.(myPhone)));
    data.(myPhone) = data.(myPhone)(rand_id);
    data.(myPhone) = data.(myPhone)(:,1:newlen);
    % initialize HMM models
    HMM.(myPhone) = initHMM(data.(myPhone), M, Q, initType);
    % train HMM models
    [HMM.(myPhone), LL] = trainHMM( HMM.(myPhone), data.(myPhone), max_iter);
end

% save HMM models
fn_HMM = 'HMM.mat';
save( fn_HMM, 'HMM', '-mat');
