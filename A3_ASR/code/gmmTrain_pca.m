function [gmms, PCvec, ratio]= gmmTrain_pca( dir_train, max_iter, epsilon, M, pca_N, deltaS)
% gmmTain_pca
% This function applies PCA to gmm training
%
%  inputs:  dir_train  : a string pointing to the high-level
%                        directory containing each speaker directory
%           max_iter   : maximum number of training iterations (integer)
%           epsilon    : minimum improvement for iteration (float)
%           M          : number of Gaussians/mixture (integer)
%           pca_N      : reduced dimentionality (integer)
%           deltaS     : the number of speakers to be excluded from the training set (integer)
%
%  output:  gmms       : a 1xN cell array. The i^th element is a structure
%                        with this structure:
%                            gmm.name    : string - the name of the speaker
%                            gmm.weights : 1xM vector of GMM weights
%                            gmm.means   : DxM matrix of means (each column 
%                                          is a vector
%                            gmm.cov     : DxDxM matrix of covariances. 
%                                          (:,:,i) is for i^th mixture
%           PCvec      : projection matrix of PCA (matrix of float)
%           ratio      : the proportion of retained variance, i. e. the ration between 
%                        the sum of the retained eigenvalues after doing PCA and the 
%                        sum of all the eigenvalues of the training matrix

    if nargin < 5
        disp('Please provide at least 5 arguments: dir_train, max_iter, epsilon, M, pca_N.');
        return;
    end
    addpath(genpath('./FullBNT-1.0.7'));
%     addpath(genpath('/h/u1/cs401/A3_ASR/code/FullBNT-1.0.7'));  % CDF path
    DD_train = dir(dir_train);
    numS = length(DD_train) - 3; % number of speakers
    deleteS = []; % indices of speakers to be deleted, if no deltaS is provided, deleteS is empty
    if nargin == 6
        numS = numS - deltaS; % if deltaS is provided, decrease numS by deltaS
        deleteS = randi([4, length(DD_train)], 1, deltaS); % randomly generate indices to be deleted
    end
    gmms = cell([numS, 1]);
    X_matrix_cat = [];
    X_matrix = cell([numS, 1]);
    k = 1;  % index of gmms, in the range of [1, numS]
    for i=1:length(DD_train) - 3
        % skip the speakers existed in deleteS
        if any(i==deleteS)
            continue;
        end
        gmms{k}.name = DD_train(i + 3).name;
        DD_mfcc = dir([dir_train, filesep, DD_train(i + 3).name, filesep, '*.mfcc']);
        path = [dir_train, filesep, DD_train(i + 3).name];
        X_matrix{k} = loadmfcc(DD_mfcc, path);
        X_matrix_cat = vertcat(X_matrix_cat, X_matrix{k});
        k = k + 1;
    end
    % PCA
    [PCcoeff0, PCvec0] = pca(X_matrix_cat, size(X_matrix_cat, 2));  % PCA with no dimensionality reduced
    [PCcoeff, PCvec] = pca(X_matrix_cat, pca_N);  % PCvec: d*d'
    ratio = sum(PCcoeff)/sum(PCcoeff0);
    
    for i=1:length(gmms)
        X_matrix_pca = X_matrix{i} * PCvec;     % X_matrix_pca: N*d'
        [omega, miu, sm] = init(M, X_matrix_pca);
        gmms{i}.weights = omega'; % (1, M)
        gmms{i}.means = miu'; % D*M
        gmms{i}.cov = sm;  % D*D*M
        % EM-step
        prev_L = -Inf;
        impr = Inf;
        j = 0;
        while j <= max_iter && impr >= epsilon
            [gmms{i}, L] = emstep(X_matrix_pca, M, gmms{i});
            impr = L - prev_L;
            prev_L = L;
            j = j + 1;
        end
    end
    
    
end

function [omega, miu, sm] = init(M, X_matrix)
    p = randperm(size(X_matrix,1));
	X_matrix = X_matrix(p,:);
    miu = X_matrix(1:M, :);
    omega =  (1 / M) * ones(M, 1);
    num_row = size(X_matrix, 2);
    sm = zeros(num_row, num_row, M);
    for i=1:M
       sm(:, :, i) = eye(num_row);
    end
end

function [gmm, L] = emstep(X_matrix, M, gmm)
    
    % e step
    T = size(X_matrix, 1);
    d = size(X_matrix, 2);
    logB = zeros(T, M);
    log_wb = zeros(T, M);
    logp = zeros(T, 1);
    
    for k=1:M
        denominator = (X_matrix - repmat(gmm.means(:,k)', T, 1)).^2; % T * d
        sgm = repmat(diag(gmm.cov(:, :, k))', T, 1);  % T*d
        logB(:, k) = -0.5 * sum(denominator./sgm, 2)- 0.5 * d * log(2 * pi) - 0.5 * sum(log(diag(gmm.cov(:, :, k)))); % T*1
        log_wb(:, k) = logB(:, k) + log(repmat(gmm.weights(k), T, 1)); % T*1
    end
    max_wb = max(log_wb, [], 2); % max of every row  T*1
    log_sum_wb = log(sum(exp(log_wb - repmat(max_wb, 1, M)), 2)) + max_wb; % T*1
    L = sum(log_sum_wb);
    
    % m step
    logP_mgivenx = log_wb - repmat(log_sum_wb, 1, M); % T*M
    sump = sum(exp(logP_mgivenx)); % 1*M
    gmm.weights = sump / T; % 1*M
    
    sumpx = (exp(logP_mgivenx)' * X_matrix)'; % d*M
    gmm.means = sumpx./ repmat(sump, d, 1); % d*M
    
    sumpx2 = (exp(logP_mgivenx)' * (X_matrix.^2))'; % d*M
    sgm = sumpx2./ repmat(sump, d, 1) - gmm.means.^2;  % d*M
    for m=1:M
        gmm.cov(:, :, m) = diag(sgm(:, m));
    end
end