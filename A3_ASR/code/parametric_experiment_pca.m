function accuracy = parametric_experiment_pca(dir_train, dir_test, max_iter, epsilon, M, pca_N, deltaS)
% parametric_experiment_pca
% This function applies PCA to the Q2 and redoes the parametric study on M, epsilon, deltaS, and pca_N.
% input :   dir_train   : the directory of training set (string)
%           dir_test    : the directory of testing set (string)
%           max_iter    : maximum number of iterations of EM-step (integer)
%           epsilon     : minimum difference of log likelihood between iterations (float)
%           M           : number of guassians (integer)
%           pca_N       : reduced dimensionality by PCA
%           deltaS      : (optional) number of speakers to be excluded from the training set 
%                         (integer) 
% output:   accuracy    : classification accuracy
    
    if nargin < 6
        disp('Please provide at least 6 arguments: dir_train, dir_test, max_iter, epsilon, M, pca_N.');
        return;
    end
    % if deltaS is not provided, set it to be 0
    if nargin == 6
        f = fopen('2.3experiment_pca.txt', 'w');
        deltaS = 0;
    elseif nargin == 7
        f = fopen('2.3experiment_S_pca.txt', 'w');
    end
    
    fprintf(f, 'deltaS\tdimension\tepsilon\t\tM\t\taccuracy\texplained_ratio\n');
    fprintf(f, '======================================================================================\n');
    for n=1:length(deltaS)
        fprintf(f, '%d\t', deltaS(n));
        for m=1:length(pca_N)
            if m ~= 1
               fprintf(f, '%d\t', deltaS(n));
            end
            fprintf(f, '%d\t\t', pca_N(m));
            for i=1:length(epsilon)
                if i ~= 1
                    fprintf(f, '%d\t', deltaS(n));
                    fprintf(f, '%d\t\t', pca_N(m));
                end
                fprintf(f, '%.6f\t', epsilon(i));
                accuracy = zeros(1, length(M));
                for j=1:length(M)
                    if j ~= 1
                        fprintf(f, '%d\t', deltaS(n));
                        fprintf(f, '%d\t\t', pca_N(m));
                        fprintf(f, '%.6f\t', epsilon(i));
                    end
                    fprintf(f, '%d\t\t', M(j));
                    [gmms, PCvec, ratio] = gmmTrain_pca( dir_train, max_iter, epsilon(i), M(j), pca_N(m), deltaS(n));
                    [class_result, accuracy(j)]= gmmClassify_pca(dir_test, M(j), gmms, PCvec);
                    fprintf(f, '%.3f\t\t%.3f\n', accuracy(j), ratio);
                end
            end
        end
    end
    fclose(f);
end