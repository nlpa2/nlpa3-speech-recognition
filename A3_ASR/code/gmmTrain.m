function gmms = gmmTrain( dir_train, max_iter, epsilon, M, deltaS)
% gmmTain
%
%  inputs:  dir_train  : a string pointing to the high-level
%                        directory containing each speaker directory
%           max_iter   : maximum number of training iterations (integer)
%           epsilon    : minimum improvement for iteration (float)
%           M          : number of Gaussians/mixture (integer)
%           deltaS     : (optional) number of speakers to be deleted (integer)
%                        this is used for 2.3, deltaS is not provided for
%                        part 2.1 and 2.2
%
%  output:  gmms       : a 1xN cell array. The i^th element is a structure
%                        with this structure:
%                            gmm.name    : string - the name of the speaker
%                            gmm.weights : 1xM vector of GMM weights
%                            gmm.means   : DxM matrix of means (each column 
%                                          is a vector
%                            gmm.cov     : DxDxM matrix of covariances. 
%                                          (:,:,i) is for i^th mixture
    if nargin < 4
        disp('Please provide at least four arguments: dir_train, max_iter, epsilon, M.');
        return;
    end
    
    DD_train = dir(dir_train);
    X_matrix_cat = [];
    numS = length(DD_train) - 3; % number of speakers
    deleteS = []; % indices of speakers to be deleted, if no deltaS is provided, deleteS is empty
    if nargin == 5
        numS = numS - deltaS; % if deltaS is provided, decrease numS by deltaS
        deleteS = randi([4, length(DD_train)], 1, deltaS); % randomly generate indices to be deleted
    end
    gmms = cell([numS, 1]);
    k = 1;  % index of gmms, in the range of [1, numS]
    for i=1:length(DD_train) - 3
        % skip the speakers existed in deleteS
        if any(i==deleteS)
            continue;
        end
        gmms{k}.name = DD_train(i + 3).name;
        DD_mfcc = dir([dir_train, filesep, DD_train(i + 3).name, filesep, '*.mfcc']);
        path = [dir_train, filesep, DD_train(i + 3).name];
        X_matrix = loadmfcc(DD_mfcc, path);
        
        % initialization
        [omega, miu, sm] = init(M, X_matrix);
        gmms{k}.weights = omega'; % (1, M)
        gmms{k}.means = miu'; % D*M
        gmms{k}.cov = sm;  % D*D*M
        
        % emstep
        prev_L = -Inf;
        impr = Inf;
        j = 0;
        while j <= max_iter && impr >= epsilon
            [gmms{k}, L] = emstep(X_matrix, M, gmms{k});
            impr = L - prev_L;
            prev_L = L;
            j = j + 1;
        end
        k = k + 1;
    end
    
end

function [omega, miu, sm] = init(M, X_matrix)
    p = randperm(size(X_matrix,1));
	X_matrix = X_matrix(p,:);
    miu = X_matrix(1:M, :);
    omega =  (1 / M) * ones(M, 1);
    num_row = size(X_matrix, 2);
    sm = zeros(num_row, num_row, M);
    for i=1:M
       sm(:, :, i) = eye(num_row);
    end
end

function [gmm, L] = emstep(X_matrix, M, gmm)
    
    % e step
    T = size(X_matrix, 1);
    d = size(X_matrix, 2);
    logB = zeros(T, M);
    log_wb = zeros(T, M);
    logp = zeros(T, 1);
    
    for k=1:M
        denominator = (X_matrix - repmat(gmm.means(:,k)', T, 1)).^2; % T * d
        sgm = repmat(diag(gmm.cov(:, :, k))', T, 1);  % T*d
        logB(:, k) = -0.5 * sum(denominator./sgm, 2)- 0.5 * d * log(2 * pi) - 0.5 * sum(log(diag(gmm.cov(:, :, k)))); % T*1
        log_wb(:, k) = logB(:, k) + log(repmat(gmm.weights(k), T, 1)); % T*1
    end
    max_wb = max(log_wb, [], 2); % max of every row  T*1
    log_sum_wb = log(sum(exp(log_wb - repmat(max_wb, 1, M)), 2)) + max_wb; % T*1
    L = sum(log_sum_wb);
    
    % m step
    logP_mgivenx = log_wb - repmat(log_sum_wb, 1, M); % T*M
    sump = sum(exp(logP_mgivenx)); % 1*M
    gmm.weights = sump / T; % 1*M
    
    sumpx = (exp(logP_mgivenx)' * X_matrix)'; % d*M
    gmm.means = sumpx./ repmat(sump, d, 1); % d*M
    
    sumpx2 = (exp(logP_mgivenx)' * (X_matrix.^2))'; % d*M
    sgm = sumpx2./ repmat(sump, d, 1) - gmm.means.^2;  % d*M
    for m=1:M
        gmm.cov(:, :, m) = diag(sgm(:, m));
    end
end