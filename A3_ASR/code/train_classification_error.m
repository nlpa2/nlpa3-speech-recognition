function error = train_classification_error(dir_train, M, epsilon, max_iter)
    gmms_cell = cell(1, 4);
    test_data = [];
    test_label = [];
    
    train_matrix = cell(1, 4);
    
    DD_train = dir(dir_train);
    DD_mfcc = dir([dir_train, filesep, DD_train(4).name, filesep, '*.mfcc']);
    [trainindex, validindex] = generate_cv(4, length(DD_mfcc), 1);
    
    for i=1:length(DD_train) - 3
        DD_mfcc = dir([dir_train, filesep, DD_train(i + 3).name, filesep, '*.mfcc']);
        p = randperm(length(DD_mfcc));
        DD_mfcc = DD_mfcc(p);  % shuffle
        path_test = [dir_train, filesep, DD_train(i + 3).name, filesep, DD_mfcc(1)]; % first one as test set
        test_data = vertcat(test_data, load(path_test));
        test_label = vertcat(test_label, DD_train(i + 3).name);
        
        train_data = cell(1, 4);
        train_label = cell(1, 4);
        valid_data = cell(1, 4);
        valid_label = cell(1, 4);
        for j=1:length(trainindex)
            gmms_cell{j}{i}.name = DD_train(i + 3).name;
%             train_label{j} = vertcat(train_label{j}, DD_train(i + 3).name);
            valid_label{j} = vertcat(valid_label{j}, DD_train(i + 3).name);
            for k=1:length(trainindex{j})
                t_index = trainindex{j}(k);
                train_path = [dir_train, filesep, DD_train(i + 3).name, filesep, DD_mfcc(t_index)];
                train_data{j} = vertcat(train_data{j}, load(train_path));
            end
            for m=1:length(validindex{j})
                v_index = validindex{j}(m);
                valid_path = [dir_train, filesep, DD_train(i + 3).name, filesep, DD_mfcc(v_index)];
                valid_data{j} = vertcat(valid_data{j}, load(valid_path));
            end
            [omega, miu, sm] = init(M, train_data{j});
            gmms_cell{j}{i}.weights = omega'; % (1, M)
            gmms_cell{j}{i}.means = miu'; % D*M
            gmms_cell{j}{i}.cov = sm;  % D*D*M
            prev_L = -Inf;
            impr = Inf;
            n = 0;
            while n <= max_iter && impr >= epsilon
                [gmms_cell{j}{i}, L] = emstep(train_data{j}, M, gmms_cell{j}{i});
                impr = L - prev_L;
                prev_L = L;
                n = n + 1;
            end
        end
    end
    
end