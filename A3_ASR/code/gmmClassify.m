function [class_result, accuracy]= gmmClassify(dir_test, M, gmms, K, likfolder)
% gmmClassify
%
%  inputs:  dir_test  : a string pointing to directory containing all the
%                       test sets
%           M         : number of Gaussians/mixture (integer)
%           gmms      : gmm models for each speaker in the training set
%                       a 1xN cell array. The i^th element is a structure
%                        with this structure:
%                            gmm.name    : string - the name of the speaker
%                            gmm.weights : 1xM vector of GMM weights
%                            gmm.means   : DxM matrix of means (each column 
%                                          is a vector
%                            gmm.cov     : DxDxM matrix of covariances. 
%                                          (:,:,i) is for i^th mixture
%           K         : the number of possible speakers returned for each
%                       classification, i. e. top K possible candidates (integer)
%           likfolder : (optional) name of the folder containing the output files (unkn_i.lik) (string)
%                       if this argument is not provided, no file is
%                       generated
%
%  output:  class_result : a cell of string containing the identified
%                          speakers for each test case
%           accuracy     : the overall accuracy of classification (float)
%
% the top-K speakers and the corresponding log likelihoods are written
% into files unkn_i.lik in the folder named likfolder

    if nargin < 3
        disp('Please input at least 3 arguments: dir_test, M, gmms.');
        return;
    end
    
    % get the test_label struct from the test ID file, e. g.
    % test_label.unkn_1 = MMMMM
    test_label = generate_test_label(dir_test);
    
    DD_mfcc = dir([dir_test, filesep, '*.mfcc']);
    class_result = cell(length(DD_mfcc), 1);
    sum = 0;
    total_num = 0;
    
    for index=1:length(DD_mfcc)
        X_matrix = loadmfcc(DD_mfcc(index), dir_test);
        P = cell([length(gmms), 2]);  % index and log likelihood of utterance uttered by each speaker
        for i=1:length(gmms)
            P{i, 1} = LogLike(X_matrix, M, gmms{i});
            P{i, 2} = gmms{i}.name;
        end
        P = sortrows(P, -1);
        class_result{index} = P{1, 2};
        if isfield(test_label, DD_mfcc(index).name(1:end-5))
            sum = sum + strcmp(class_result{index}, test_label.(DD_mfcc(index).name(1:end-5)));
            total_num = total_num + 1;
        end
        if nargin == 4
            disp('Failed to create documents. Please input the name of the folder.');
        end
        if nargin == 5
            if exist(likfolder, 'dir') == 0
                mkdir(likfolder);
            end

            filename = strcat(DD_mfcc(index).name(1:end-5), '.lik');
            filepath = [likfolder, filesep, filename];
            f = fopen(filepath, 'w');
            fprintf(f, 'utterance: %s\n', DD_mfcc(index).name);
            for j=1:K
                fprintf(f, '%d %1.5f %s\n', j, P{j, :});
            end
            fclose(f);
        end
    end
    accuracy = sum / total_num;
    
end
