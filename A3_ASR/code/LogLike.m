function L = LogLike(X_matrix, M, gmm)
% LogLike
% This function computes the log likelihood of a given gmm model
% input :   X_matrix    : the concatenated matrix of mfcc files (matrix of float)
%           M           : the number of guassians (integer)
%           gmm         : guassian mixture model (struct)
% output:   L           : log likelihood (float)
    % e step
    T = size(X_matrix, 1);
    d = size(X_matrix, 2);
    logB = zeros(T, M);
    log_wb = zeros(T, M);
    logp = zeros(T, 1);
    
    for k=1:M
        denominator = (X_matrix - repmat(gmm.means(:,k)', T, 1)).^2; % T * d
        sgm = repmat(diag(gmm.cov(:, :, k))', T, 1);  % T*d
        logB(:, k) = -0.5 * sum(denominator./sgm, 2)- 0.5 * d * log(2 * pi) - 0.5 * log(det(gmm.cov(:, :, k))); % T*1
        log_wb(:, k) = logB(:, k) + log(repmat(gmm.weights(k), T, 1)); % T*1
    end
    max_wb = max(log_wb, [], 2); % max of every row  T*1
    log_sum_wb = log(sum(exp(log_wb - repmat(max_wb, 1, M)), 2)) + max_wb;
    L = sum(log_sum_wb); 
end