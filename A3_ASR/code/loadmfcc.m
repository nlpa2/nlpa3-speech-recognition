function X_matrix = loadmfcc(DD_mfcc, path)
% loadmfcc
% this function loads the mfcc files from the provided path
% input	:	DD_mfcc	: the directory of mfcc files (string)  'dir/filename/*.mfcc'
%			path    : the directory of the folder containing mfcc fiels (string) 'dir/filename'
% output:	X_matrix: the catenated matrix of all the mfcc files (matrix of float)
% 
    X_matrix = [];
    for index=1:length(DD_mfcc)
        pathmfcc = [path, filesep, DD_mfcc(index).name]; % dir/filename/DD_mfcc(index).name
        X_matrix = vertcat(X_matrix, load(pathmfcc));
    end
end