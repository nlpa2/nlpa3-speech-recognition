function accuracy = parametric_experiment(dir_train, dir_test, max_iter, epsilon, M, deltaS)
% parametric_experiment
% This funciton does the parametric study on differen M, epsilon, and deltaS.
% Results are written into 2.3experiment.txt
%  inputs:  dir_test  : a string pointing to directory containing all the
%                       test sets
%           dir_train : a string pointing to the high-level
%                        directory containing each speaker directory
%           max_iter   : maximum number of training iterations (integer)
%           epsilon    : minimum improvement for iteration (array of float or float)
%           M          : number of Gaussians/mixture (array of integer or integer)
%           deltaS     : (optional)number of speakers to be deleted (array of integer or integer)
%
%  output: accuracy    : the overall accuracy of each classification (array of float)

    % write the parametric study result to 2.3experiment.txt
    if nargin < 5
        disp('Please provide at least 5 arguments: dir_train, dir_test, max_iter, epsilon, M.');
        return;
    end
    % if deltaS is not provided, set it to be 0
    if nargin == 5
        f = fopen('2.3experiment.txt', 'w');
        deltaS = 0;
    elseif nargin == 6
        f = fopen('2.3experiment_S.txt', 'w');
    end
    fprintf(f, 'deltaS\tepsilon\t\tM\t\taccuracy\n');
    fprintf(f, '=====================================================\n');
    for n=1:length(deltaS)
        fprintf(f, '%d\t', deltaS(n));
        for i=1:length(epsilon)
            if i ~= 1
               fprintf(f, '%d\t', deltaS(n));
            end
            fprintf(f, '%.6f\t', epsilon(i));
            accuracy = zeros(1, length(M));
            for j=1:length(M)
                if j ~= 1
                    fprintf(f, '%d\t', deltaS(n));
                    fprintf(f, '%.6f\t', epsilon(i));
                end
                fprintf(f, '%d\t\t', M(j));
                gmms = gmmTrain( dir_train, max_iter, epsilon(i), M(j), deltaS(n));
                if length(deltaS) > 1
                    [class_result, accuracy(j)]= gmmClassify(dir_test, M(j), gmms, 5, strcat('parametric_lik',... 
                    num2str((n-1)*length(epsilon)*length(M) + (i-1)*length(M) + j)));
                else
                    [class_result, accuracy(j)]= gmmClassify(dir_test, M(j), gmms);
                end
%                 fprintf(f, 'accuracy: ');
                fprintf(f, '%.3f\n', accuracy(j));
            end
%             fprintf(f, '\n\t');
        end
%         fprintf(f, '\n');
    end
    fclose(f);
end