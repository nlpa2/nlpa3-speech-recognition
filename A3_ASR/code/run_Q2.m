% run the following codes for 2.1, 2.2, and 2.3. 
% 2.1
dir_train = '/u/cs401/speechdata/Training';
dir_test = '/u/cs401/speechdata/Testing';
max_iter = 50;
epsilon = 1e-5;
M = 8;
gmms = gmmTrain( dir_train, max_iter, epsilon, M);

% 2.2
% Results are written into folder named 'likelihoods'
K = 5;
[class_result, accuracy]= gmmClassify(dir_test, M, gmms, K, 'likelihoods');

% 2.3
% fix M and epsilon, explore different deltaS
% results are written into '2.3experiment_S.txt'
deltaS = [4, 8, 16, 20];
accuracy = parametric_experiment(dir_train, dir_test, max_iter, epsilon, M, deltaS);

% Explore different epsilon and M using all speakers (no deltaS)
% Results are written into '2.3experiment.txt'
epsilon = [0.1, 1e-3, 1e-5];
M = [2, 4, 8, 12];
accuracy = parametric_experiment(dir_train, dir_test, max_iter, epsilon, M);