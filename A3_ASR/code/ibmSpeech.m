test_dir = '/u/cs401/speechdata/Testing';

% Speech-to-text BlueMix 
username1 = '0820cd82-f1d1-4c61-ab5b-3a123ed2f94c';
password1 = 'XCmtYlIbw0KI';

f1 = fopen('ibm_transcripts4-1.txt', 'w');
transcript = {};
flac = dir([test_dir, filesep, '*.flac']);
flac = {flac.name};
for i = 1:length(flac)
    % read unkn_*.flac in numerical order
    path = [test_dir, filesep, 'unkn_', num2str(i), '.flac'];
    % call IBM Speech-to-text services
    [status, result] = unix(['env LD_LIBRARY_PATH="" curl -u ',username1,':',password1,' -X POST --header "Content-Type: audio/flac" --header "Transfer-Encoding: chunked" --data-binary @', path, ' "https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?continuous=true"']);
    % call the third-party json parsing function to extract the transcript
    % field
    data = parse_json(result);
    transcript{i} = data.results{1}.alternatives{1}.transcript;
    disp(transcript{i});
    % store the transcript begin with 2 numbers (for the convenience of Levenshtein function)
    fprintf(f1,[num2str(i),' 00 ', transcript{i},'\n']);
end
fclose(f1);
% result stored as WER_4-1.txt
hypothesis = 'ibm_transcripts4-1.txt';
annotation_dir = test_dir;
[SE IE DE LEV_DIST] = Levenshtein(hypothesis,annotation_dir);


% Speech-to-text BlueMix 
username2 = 'dffe915d-aa22-487a-8aec-83ec485c0cd4';
password2 = 'FhoemIAgUVCn';

unkn_lik_dir = '.';
unkn_lik = dir([unkn_lik_dir, filesep, '*.lik']);
% remove . and .. directory
unkn_lik = unkn_lik(arrayfun(@(x) ~strcmp(x.name(1),'.'),unkn_lik));
unkn_lik = {unkn_lik.name};

f2 = fopen('ibm_transcripts4-2.txt', 'w');
transcript = {};
for i = 1:length(unkn_lik)
    % open unkn_*.txt in numerical order
    [temp1, temp2, text] = textread([test_dir, filesep, 'unkn_', num2str(i), '.txt'],'%d %d %s', 'delimiter', '\n');
    % remove single quote in the sentence : Don't -> don't 
    % for the reason of unix curl quote match
    text = regexprep(text{1},'\''','');
    % construct the json text
    text = ['''{"text":"', text, '"}'''];
    % read corresponding unkn_*.lik and fetch the top guessed speaker's ID
    [ll1, ll2, flag] = textread([unkn_lik_dir, filesep, 'unkn_', num2str(i),'.lik'], '%d.%d %s', 'delimiter', '\n', 'headerlines', 1);
    flag = flag{1};
    % if the ID begins with M -> male, else if it begins with F -> female
    if strcmp(flag(1), 'M') == 1
        voice = 'en-US_MichaelVoice';
    else
        voice = 'en-US_LisaVoice';
    end
    % call IBM Text-to-speech services
    [status, result] = unix(['env LD_LIBRARY_PATH="" curl -u ',username2,':',password2,' -X POST --header "Content-Type: application/json" --data ', text,' "https://stream.watsonplatform.net/text-to-speech/api/v1/synthesize?accept=audio%2Fflac&voice=', voice,'" > ibm_', num2str(i), '.flac']);    
    path = ['ibm_', num2str(i), '.flac'];
    % call IBM Speech-to-text services again
    [status, result] = unix(['env LD_LIBRARY_PATH="" curl -u ',username1,':',password1,' -X POST --header "Content-Type: audio/flac" --header "Transfer-Encoding: chunked" --data-binary @', path, ' "https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?continuous=true"']);
    data = parse_json(result);
    transcript{i} = data.results{1}.alternatives{1}.transcript;
    fprintf(f2,[num2str(i),' 00 ', transcript{i},'\n']);
end
fclose(f2);
% result stored as WER_4-2.txt
hypothesis = 'ibm_transcripts4-2.txt';
annotation_dir = test_dir;
[SE IE DE LEV_DIST] = Levenshtein(hypothesis,annotation_dir);
