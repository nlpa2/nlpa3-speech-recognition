function [SE IE DE LEV_DIST] =Levenshtein(hypothesis,annotation_dir)
% Input:
%	hypothesis: The path to file containing the the recognition hypotheses
%	annotation_dir: The path to directory containing the annotations
%			(Ex. the Testing dir containing all the *.txt files)
% Outputs:
%	SE: proportion of substitution errors over all the hypotheses
%	IE: proportion of insertion errors over all the hypotheses
%	DE: proportion of deletion errors over all the hypotheses
%	LEV_DIST: proportion of overall error in all hypotheses

% read hypotheses.txt file line by line
[NoUse1, NoUse2, hypo_sens] = textread(hypothesis,'%d %d %s', 'delimiter', '\n');

% some initial values
SE = 0;
IE = 0;
DE = 0;
LEV_DIST = 0;
total_words = 0;

% R is the matrix of distances
% B is the backtracking matrix
R = {};
B = {};
f = fopen(['./WER_',hypothesis(end-6:end)],'w');
for l = 1:length(hypo_sens)
    % convert the sentence to lowercase, remove all the punctuations
    hypo_list = preprocessing(hypo_sens{l});
    % read unkn_*.txt in numerical order
    [NoUse1, NoUse2, anno_sens] = textread([annotation_dir, filesep,'unkn_', num2str(l), '.txt'],'%d %d %s', 'delimiter', '\n');
    % do the same preprocessing as the hypothesis sentence
    anno_list = preprocessing(anno_sens{1});
    
    n = length(anno_list);
    m = length(hypo_list);
    total_words = total_words + n;
    R{l} = zeros(n+1, m+1);
    B{l} = cell(n+1, m+1);
    
    % initialize R[0,0] = 0, and R[i,j] = Inf for all other i = 0 or j = 0
    R{l}(:, 1) = Inf;
    R{l}(1, :) = Inf;
    R{l}(1, 1) = 0;
    
    for i = 2:n+1 % reference words
        for j = 2:m+1 % hypothesis words
            del = R{l}(i-1, j) + 1;
            sub = R{l}(i-1, j-1) + ~strcmp(anno_list{i-1}, hypo_list{j-1});
            ins = R{l}(i, j-1) + 1;
            R{l}(i, j) = min([del;sub;ins]);
            if R{l}(i, j) == del
                B{l}{i, j} = 'up';      % deletion error
            elseif R{l}(i, j) == ins
                B{l}{i, j} = 'left';    % insertion error
            else
                B{l}{i, j} = 'up-left';  % substitution error 
            end
        end
    end
    
    % backtrace
    i = n+1;
    j = m+1;
    SE_single = 0;
    IE_single = 0;
    DE_single = 0;
    
    while i~=1 && j~=1
        % deletion error
        if strcmp(B{l}{i, j},'up') == 1
            DE = DE + 1;
            DE_single = DE_single + 1;
            i = i - 1;
        % insertion error
        elseif strcmp(B{l}{i, j},'left') == 1
            IE = IE + 1;
            IE_single = IE_single + 1;
            j = j - 1;
        else
            % substitution error or match
            if strcmp(anno_list{i-1}, hypo_list{j-1}) == 0
                SE = SE + 1;
                SE_single = SE_single + 1;
            end
            i = i - 1;
            j = j - 1;
        end
    end
    % calculate SE IE DE LD for each utterance
    LD_single = R{l}(n+1, m+1)/n;
    LEV_DIST = LEV_DIST + R{l}(n+1, m+1); 
    fprintf(f,'Reference: %s \n', anno_sens{1});
    fprintf(f,'Hypothsis: %s \n', hypo_sens{l});
    fprintf(f,'SE: %f  IE: %f  DE: %f \n', SE_single/n, IE_single/n, DE_single/n);
    fprintf(f,'LD for each sentence: %f \n', LD_single);
    fprintf(f,'=============================================\n');
end 

% calculate SE IE DE LD for all the utterances
SE = SE/total_words;
IE = IE/total_words;
DE = DE/total_words;
LEV_DIST = LEV_DIST / total_words;
fprintf(f,'SE overall: %f\n', SE);
fprintf(f,'IE overall: %f\n', IE);
fprintf(f,'DE overall: %f\n', DE);
fprintf(f,'WER overall: %f\n',LEV_DIST);
end

function [wordlist] = preprocessing(line)
    % convert to lowercase
    line = lower(line);
    % remove single quote
    line = regexprep(line,'\''','');
    % split by all the other punctuations and space
    wordlist = regexp(line, '[^\-_a-z0-9]', 'split');
    % remove empty elements in the cell array
    wordlist = wordlist(~cellfun('isempty',wordlist));    
end
